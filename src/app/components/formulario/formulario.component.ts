import { Component, OnInit } from '@angular/core';
import {  FormBuilder,FormGroup,Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';
// import { ValidadoresService } from 'src/app/services/validadores.service';




@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  forma!: FormGroup;

  constructor( private fb: FormBuilder,
                private validador: ValidadoresService) { 
  this.crearFormulario();
  this.cargarDataAlFormulario();
                }

  ngOnInit(): void {
  }
  get nombreNovalido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched
  }
  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched
  } 

  crearFormulario():void{
    this.forma = this.fb.group({
      nombre:['', [Validators.required, Validators.minLength(25), Validators.pattern(/^[a-zA-ZñÑ\s]+$/), this.validador]],
      correo:['',[Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
    })
  }
  cargarDataAlFormulario():void{
    this.forma.setValue({
      nombre:'',
      correo:''
    })
  } 


  guardar(): void{

  }

  

}
